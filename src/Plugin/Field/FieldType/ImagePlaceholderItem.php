<?php

namespace Drupal\jsonapi_image_placeholder\Plugin\Field\FieldType;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Field\Plugin\Field\FieldType\StringItem;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;

/**
 * Image placeholder
 *
 * @FieldType(
 *   id = "image_placeholder",
 *   label = @Translation("Image placeholder"),
 *   description = @Translation("Image placeholder in base64 format"),
 *   no_ui = TRUE,
 *   list_class = "\Drupal\jsonapi_image_placeholder\ImagePlaceholderItemList",
 * )
 */
class ImagePlaceholderItem extends StringItem {

  /**
   * Whether or not the value has been calculated.
   *
   * @var bool
   */
  protected $isCalculated = FALSE;

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'max_length' => 2500,
      'is_ascii' => TRUE,
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function __get($name) {
    $this->ensureCalculated();
    return parent::__get($name);
  }
  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $this->ensureCalculated();
    return parent::isEmpty();
  }

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    $this->ensureCalculated();
    return parent::getValue();
  }

  /**
   * Calculates the value of the field and sets it.
   */
  protected function ensureCalculated() {
    if ($this->isCalculated) {
      return;
    }
    $entity = $this->getEntity();
    $uri = ($entity instanceof File && substr($entity->getMimeType(), 0, 5) === 'image')
      ? $entity->getFileUri()
      : FALSE;
    if (!$uri) {
      return;
    }
    /** @var \Drupal\Core\Cache\CacheBackendInterface $cacheBackend */
    $cacheBackend = \Drupal::service('cache.jsonapi_image_placeholder');
    $cacheItem = $cacheBackend->get($uri);
    if ($cacheItem) {
      $this->setValue($cacheItem->data);
      $this->isCalculated = TRUE;
      return;
    }
    $imgStyle = ImageStyle::load('thumbnail');
    $entity->addCacheableDependency($imgStyle);
    $derivativeUri = $imgStyle->buildUri($uri);
    if (!file_exists($derivativeUri)) {
      $imgStyle->createDerivative($uri, $derivativeUri);
    }
    /** @var \Drupal\Core\Image\ImageFactory $imageFactory */
    $imageFactory = \Drupal::service('image.factory');
    $processedImage = $imageFactory->get($derivativeUri);
    $data = file_get_contents($derivativeUri);
    if (!$data) {
      return;
    }
    $base64 = 'data:' . $processedImage->getMimeType() . ';base64,' . base64_encode($data);
    $cacheTags = [
      ...$entity->getCacheTags(),
      ...$imgStyle->getCacheTags(),
    ];
    $cacheBackend->set($uri, $base64, Cache::PERMANENT, $cacheTags);
    $this->setValue($base64);
    $this->isCalculated = TRUE;
  }
}
