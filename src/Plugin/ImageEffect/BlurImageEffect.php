<?php

namespace Drupal\jsonapi_image_placeholder\Plugin\ImageEffect;

use Drupal\Core\Image\ImageInterface;
use Drupal\image\ImageEffectBase;

/**
 * Crops an image resource.
 *
 * @ImageEffect(
 *   id = "jsonapi_image_placeholder_crop",
 *   label = @Translation("Blur"),
 *   description = @Translation("Blur image.")
 * )
 */
class BlurImageEffect extends ImageEffectBase {

  public function applyEffect(ImageInterface $image) {
    $image->getToolkit()->apply('blur', []);
  }


}
