<?php

namespace Drupal\jsonapi_image_placeholder\Plugin\ImageToolkit\Operation\gd;

use Drupal\system\Plugin\ImageToolkit\Operation\gd\GDImageToolkitOperationBase;

/**
 * Defines GD2 blur operation.
 *
 * @ImageToolkitOperation(
 *   id = "jsonapi_image_placeholder_blur",
 *   toolkit = "gd",
 *   operation = "blur",
 *   label = @Translation("Blur"),
 *   description = @Translation("Instructs the toolkit to blur the image.")
 * )
 */
class Blur extends GDImageToolkitOperationBase {

  protected function arguments() {
    return [];
  }

  protected function execute(array $arguments) {
    $resource = $this->getToolkit()->getResource();
    for ($i = 0; $i < 10; $i++) {
      $result = imagefilter($resource, IMG_FILTER_GAUSSIAN_BLUR);
      if (!$result) {
        break;
      }
    }
    return $result;
  }


}
